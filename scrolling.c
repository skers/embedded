/**
 * Scrolling text test
 * skers@kth.se 2013
 */
#include <stdio.h>
#define LINEWIDTH 10

void print_queue(char *queue)
{
	int i;
	for (i = 0; i < LINEWIDTH; i += 1) {
		printf("%c", queue[i]);
	}

	printf("\n");
}

void scroll_left(char *queue)
{
	int i;
	for (i = 0; i < LINEWIDTH-1; i += 1) {
		queue[i] = queue[i+1];
	}
	queue[LINEWIDTH-1] = '_';
}

char read_char(void)
{
	char buf[64];
	scanf("%s", buf);
	return buf[0];
}

int main(void)
{
	char ch = ' ';
	char queue[LINEWIDTH] = "";
	printf("Type 'q' to quit\n");
	print_queue(queue);

	int pos = 0;
	while (ch != 'q') {
		ch = read_char();

		if (pos == LINEWIDTH) {
			/* end of line */
			scroll_left(queue);
			queue[pos-1] = ch;
		} else {
			/* moving caret */
			queue[pos] = ch;
			pos += 1;
		}

		print_queue(queue);
	}

	return 0;
}

