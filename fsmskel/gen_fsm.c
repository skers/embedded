/* Funky Random Functional Finite State Machine
 * Kers/Wikmans
 * KTH 2013
 */

/* Kriterier
 * Man skall kunna sända data bytevis seriellt från en PC‐dator1 till labkortet2. 
 * Man skall från en dator kunna påverka fyra digitala utgångar på labkortet så att en enskild 
 * utgång skall kunna ettställas eller nollställas utan att påverka övriga utgångar. Kvittens skall 
 * skickas till datorn. 
 * Man skall från datorn kunna läsa temperaturen på labkortet. 
 * Det skall på displayen visas ett statusmeddelande på vilken funktion som utförs. 
 * Huvudprogrammet skall uppdatera en räknare (varje systick) och visa värdet på displayen. 
 * Programmet får inte ligga och vänta på seriedata in (polling). När data kommer in på 
 * serieporten skall data tas om hand med avbrottshantering. 
 * Programmet kan lämpligen skrivas som en tillståndsmaskin. 
 * Programmet skall använda sig av Peripheral’s drivers API3 ur STM32F10x Standard Peripheral s Library. 
 * Programmet kan lämpligen skrivas som en tillståndsmaskin. 
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#include "fsm_main.h"

// State functions
void state1(state_data *s); 
void state2(state_data *s);
void state3(state_data *s);
void terminate(state_data *s);

// Routine functions
void init(state_data *s);
void set_state(state_data *s, const char *name, void(*next)());
void poll_some_sensor();
void do_cool_shit();

int main() {
	state_data *s = NULL;
	s = malloc(sizeof(state_data)); 
	init(s);
	while(s->run) {
		s->next(s);
	}
	free(s);
	return 0;
}

void set_state(state_data *s, const char *name, void(*next_state)()){
	memcpy(s->function_name, name, strlen(name));
	s->next = next_state;
}

void init(state_data *s) {
	set_state(s, "init", state1);
	srand(time(NULL));
	s->run = 1;
	printf("[%s] stuff initiated\n", s->function_name);
}

void state1(state_data *s) {
	set_state(s, "state1", state1);
	if (rand() % 100 < 1) {
		printf("[%s] yez\n", s->function_name);
		s->next = state2;
	}
}

void state2(state_data *s) {
	set_state(s, "state2", state1);
	if (rand() % 20 < 1) {
		printf("[%s] almost\n", s->function_name);
		s->next = state3;
	} 
}

void state3(state_data *s) {
	set_state(s, "state3", state1);
	if (rand() % 30 < 1) {
		printf("[%s] you did it!\n", s->function_name);
		s->next = terminate;
	} else { 
		printf("[%s] nope\n", s->function_name);
	}
}

void terminate(state_data *s) {
	set_state(s, "terminate", terminate);
	printf("[%s] hasta la vista\n", s->function_name);
	s->run = 0;
}
