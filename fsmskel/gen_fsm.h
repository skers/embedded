#ifndef fsm_main_h
#define fsm_main_h
typedef struct {
	int run;
	char function_name[16];
	char glcd[64];
	void(*next)();
} state_data;
#endif
