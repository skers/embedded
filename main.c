/*************************************************************************
 * Embedded Systems
 * Lab assignment 2
 * 
 * skers@kth.se
 * wikmans@kth.se
 * KTH 2013
 **************************************************************************/
#include <string.h>
#include "includes.h"
#include "stm32f10x.h"
#include "stm32f10x_usart.h"
#include "temp_sensor_drv.h"
//#include "stm32_eval.h"

#define DLY_100US       450
#define LED1_MASK       GPIO_Pin_6
#define LED2_MASK       GPIO_Pin_7
#define LED3_MASK       GPIO_Pin_8
#define LED4_MASK       GPIO_Pin_9
#define LED_MASK        LED1_MASK | LED2_MASK | LED3_MASK | LED4_MASK
#define LEDS_PORT       GPIOF

#define LINEWIDTH       19

extern FontType_t Terminal_6_8_6;
extern FontType_t Terminal_9_12_6;
extern FontType_t Terminal_18_24_12;

Int32U CriticalSecCntr;
volatile Boolean SysTickFl;

void TickHandler(void)
{
  SysTickFl = TRUE;
}

/*************************************************************************
 * Function Name: DelayResolution100us
 * Parameters: Int32U Dly
 *
 * Return: none
 *
 * Description: Delay ~ (arg * 100us)
 *
 *************************************************************************/
void DelayResolution100us(Int32U Dly)
{
  for(; Dly; Dly--)
  {
    for(volatile Int32U j = DLY_100US; j; j--)
    {
    }
  }
}

Boolean led_toggle(Boolean state, int led)
{
  printf("LED %d toggled\n", led);
  if (state) {
    GPIO_WriteBit(LEDS_PORT,led,Bit_SET);
  }
  else {
    GPIO_WriteBit(LEDS_PORT,led,Bit_RESET);
  }
  return !state;

}

void print_queue(char *queue)
{
  GLCD_TextSetPos(0,-8);
  int i;
  for (i = 0; i < LINEWIDTH; i += 1) {
    GLCD_print("%c", queue[i]);
  }
}

void scroll_left(char *queue)
{
	int i;
	for (i = 0; i < LINEWIDTH-1; i += 1) {
		queue[i] = queue[i+1];
	}
	queue[LINEWIDTH-1] = '_';
}

void USART1_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;
  USART_InitTypeDef USART_InitStructure;

  /* Configure USART1 Tx (PB6) as alternate function push-pull */
  GPIO_PinRemapConfig(GPIO_Remap_USART1, ENABLE);
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
  GPIO_Init(GPIOB, &GPIO_InitStructure);

  /* Configure USART1 Rx (PB6) as input floating */
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_7;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
  GPIO_Init(GPIOB, &GPIO_InitStructure);
  GPIO_PinRemapConfig(GPIO_Remap_USART1, ENABLE);

  /* USART1 configured as follow:
     - BaudRate = 115200 baud
     - Word Length = 8 Bits
     - One Stop Bit
     - No parity
     - HW flow control disabled
     - Receive and transmit enabled
  */
  USART_InitStructure.USART_BaudRate = 115200;
  USART_InitStructure.USART_WordLength = USART_WordLength_8b;
  USART_InitStructure.USART_StopBits = USART_StopBits_1;
  USART_InitStructure.USART_Parity = USART_Parity_No;
  USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
  USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
  USART_Init(USART1, &USART_InitStructure);
  USART_ITConfig(USART1, USART_IT_RXNE | USART_IT_TXE, ENABLE);
  
//  STM_EVAL_COMInit(0, &USART_InitStructure);
  printf("\n\rUSART Printf Example: retarget the C library printf function to the USART\n\r");

  /* Enable the USART1 */
  USART_Cmd(USART1, ENABLE);

//  ReturnFunc();

  /* Enable EXTI for the menu navigation keys  */
//  IntExtOnOffConfig(ENABLE);
}

void to_hyperterminal(const char *string) {
  int i;
  while (*string != '\0') {
    USART_SendData(USART1, *string++);
    while (USART_GetFlagStatus(USART1, USART_FLAG_TXE) == RESET);
  }
}

#include <time.h>
#include <stdlib.h>

void USART1Handler(void)
{
  // Booleans for toggling
  static Boolean led_state[4] = {FALSE, FALSE, FALSE, FALSE};
  
  static uint8_t ch = ' ';
  static uint8_t queue[LINEWIDTH] = "                   ";
  print_queue(queue);
  
  GLCD_TextSetPos(0,-8);
  GLCD_print("%s",(char) rand() % 10);
  static int pos = 0; /* Cursor position */
  
  // Read character from USART
  if (USART_GetFlagStatus(USART1, USART_FLAG_RXNE) != RESET) { // New input?
    ch = USART_ReceiveData(USART1);
    
    switch (ch) {
    case '1':
      led_state[0] = led_toggle(led_state[0], LED1_MASK);
      to_hyperterminal("Led 1 toggled    ");
      break;
    case '2':
      led_state[1] = led_toggle(led_state[1], LED2_MASK);
      to_hyperterminal("Led 2 toggled    ");
      break;
    case '3':
      led_state[2] = led_toggle(led_state[2], LED3_MASK);
      to_hyperterminal("Led 3 toggled    ");
      break;
    case '4':
      led_state[3] = led_toggle(led_state[3], LED4_MASK);
      to_hyperterminal("Led 4 toggled    ");
      break;
    default:
      GLCD_TextSetPos(0,0);
      
      if (pos == LINEWIDTH) {
        /* end of line */
        scroll_left(queue);
        queue[pos-1] = ch;
      } else {
        /* moving caret */
        queue[pos] = ch;
        pos += 1;
      }
      print_queue(queue);
      break;
    }
  }
}

/* NVIC module configuration */
void NVIC_Configuration(void)
{
  NVIC_InitTypeDef NVIC_InitStructure;
  /* Place the vector table into FLASH */
  NVIC_SetVectorTable(NVIC_VectTab_FLASH, 0x0);
  /* Enabling interrupt from USART1 */
  NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
}

void Temp_Init(void)
{
  // Init Temperature sensor
  if(   FALSE == TempSensor_Init()
     || FALSE == TempSensorShutdown(FALSE)
     || FALSE == TempSensor_Conf(31.0,30.0,TEMP_SENOSR_COMP_MODE,2))
  {
    // Initialization fault
    GLCD_TextSetPos(0,0);
    GLCD_print("\fSTCN75 Init.\r\nfault\r\n");
    while(1);
  }
}

void main(void)
{
    Flo32 Temp;
    Boolean Alarm = 0;
//    seed(time(NULL));
    ENTR_CRT_SECTION();
    /* Setup STM32 system (clock, PLL and Flash configuration) */
    SystemInit();

    /* Enable DMA1 clock */
    RCC_AHBPeriphClockCmd (RCC_AHBPeriph_DMA1, ENABLE);

    /* Enable DAC, USART2, TIM2, PWR, CAN and BKP clocks */
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_DAC    | RCC_APB1Periph_TIM2 | RCC_APB1Periph_TIM3 |
            RCC_APB1Periph_USART2 | RCC_APB1Periph_PWR   | RCC_APB1Periph_SPI2   |
            RCC_APB1Periph_BKP, ENABLE);


    /* Enable GPIOx (x = A, B, C, D, E, F, G) ADC1, USART1, SPI1 and AFIO clocks */
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOB |
            RCC_APB2Periph_GPIOC | RCC_APB2Periph_GPIOD |
            RCC_APB2Periph_GPIOE | RCC_APB2Periph_GPIOF |
            RCC_APB2Periph_USART1 | RCC_APB2Periph_GPIOG | RCC_APB2Periph_AFIO  |
            RCC_APB2Periph_ADC1  | RCC_APB2Periph_SPI1,ENABLE);  //RCC_APB2Periph_GPIO_CS|
    /* Enable PWR and BKP clock */

    // NVIC init
#ifndef  EMB_FLASH
    /* Set the Vector Table base location at 0x20000000 */
    NVIC_SetVectorTable(NVIC_VectTab_RAM, 0x0);
#else  /* VECT_TAB_FLASH  */
    /* Set the Vector Table base location at 0x08000000 */
    NVIC_SetVectorTable(NVIC_VectTab_FLASH, 0x0);
#endif
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);

    // SysTick end of count event each 0.5s with input clock equal to 9MHz (HCLK/8, default)
    SysTick_Config(4500000);
    SysTick_CLKSourceConfig(SysTick_CLKSource_HCLK_Div8);

    // GPIO enable clock and release Reset
    GPIO_InitTypeDef GPIO_InitStructure;
    RCC_APB2PeriphResetCmd(RCC_APB2Periph_GPIOF, DISABLE);
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOF, ENABLE);
    // Status LED port init
    GPIO_InitStructure.GPIO_Pin =  LED_MASK;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(LEDS_PORT, &GPIO_InitStructure);

    GPIO_WriteBit(LEDS_PORT,LED1_MASK,Bit_RESET);

    EXT_CRT_SECTION();

    // GLCD init
    GLCD_PowerUpInit(0x0);
    GLCD_Backlight(BACKLIGHT_ON);
    GLCD_SetFont(&Terminal_9_12_6,0x000F00,0x00FF0);
    GLCD_SetWindow(10,104,131,131);
    GLCD_TextSetPos(2,10);
    printf("STM32F103ZE-SK\r");

//    Temp_Init();
    USART1_Init();
    NVIC_Configuration();

    // Print to LCD
    GLCD_SetFont(&Terminal_9_12_6,0x10,0xFFF);
    GLCD_TextSetPos(0,0);
    GLCD_print("Douche Data");
    
    while (1) {
      if (SysTickFl) {
        SysTickFl = FALSE;
        
        if(TempSensorGetTemp(&Temp, &Alarm)) {
          GLCD_TextSetPos(0,0);
          GLCD_print("\fTemperature: %3.1fC", Temp);
        }
      }
    }
}
